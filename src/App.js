import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import Users from './Users'
import Signup from './Signup'
import Profile from './Profile'
import EditProfile from './EditProfile'
import './App.css'

const App = () => {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact>
            Welcome! Go ahead and <Link to="/signup">sign up</Link>!
            <Users />
          </Route>
          <Route path="/users/:id" exact>
            <Profile />
          </Route>
          <Route path="/users/:id/edit" exact>
            <EditProfile />
          </Route>
          <Route path="/signup" exact>
            <Signup />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App
