import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

const Users = () => {
    const [ users, setUsers ] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/users')
            .then(r => r.json())
            .then(data => setUsers(data))
    }, [])

    if (!users) return 'Loading...'

    return (
        <div id="users">
            {users.map(user =>
                <Link to={`/users/${user.id}`} key={user.id}>
                    <div className="user">
                        <img src={user.avatar} alt="avatar" width="100" height="100" />
                        <h1>{user.name}</h1>
                    </div>
                </Link>
            )}
        </div>
    )
}

export default Users
