import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'

const Profile = () => {
    const { id } = useParams()

    const [ user, setUser ] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/users/' + id)
            .then(r => r.json())
            .then(data => setUser(data))
    }, [id])

    if (!user) return 'Loading...'

    return (
        <div id="profile">
            <img src={user.avatar} alt="avatar" />
            <h1>{user.name}</h1>
            <p>{user.title} at {user.company}</p>
            <p>{user.bio}</p>
        </div>
    )
}

export default Profile
