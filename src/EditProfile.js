import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import './Signup.css'

const EditProfile = () => {
    const { id } = useParams()
    const [ user, setUser ] = useState()
    const handler = field => e => setUser({ ...user, [field]: e.target.value })
    useEffect(() => {
        fetch('http://localhost:8080/users/' + id)
            .then(r => r.json())
            .then(data => setUser(data))
    }, [id])

    const history = useHistory()
    const [isError, setError] = useState(false)

    const handleSubmit = async (e) => {
        e.preventDefault()
        setError(false)
        try {
            const ret = await fetch('http://localhost:8080/users/' + id, {
                method: 'PUT',
                body: JSON.stringify(user),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            await ret.json()
            history.push(`/users/${id}`)
        } catch (err) {
            console.warn('Error:', err)
            setError(true)
        }
    }

    if (!user) return 'Loading...'

    return (
        <form id="signup" onSubmit={handleSubmit}>
            <label>
                Name:
                <input name="name" required value={user.name} onChange={handler('name')} />
            </label>
            <label>
                Birthday:
                <input name="birthday" type="date" required value={user.birthday} onChange={handler('birthday')} />
            </label>
            <label>
                Title:
                <input name="title" required value={user.title} onChange={handler('title')} />
            </label>
            <label>
                Company:
                <input name="company" required value={user.company} onChange={handler('company')} />
            </label>
            <label>
                Biography:
                <textarea name="bio" required value={user.bio} onChange={handler('bio')} />
            </label>
            <label>
                Avatar:
                <input name="avatar" type="url" required value={user.avatar} onChange={handler('avatar')} />
            </label>
            <button>Sign up!</button>
            {isError && <div>Error, please try again</div>}
        </form>
    )
}

export default EditProfile
